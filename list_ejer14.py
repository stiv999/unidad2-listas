# Read a 2D list of integers:
d = input().split()
NUM_ROWS = int(d[0])
NUM_COLS = int(d[1])
a = [[int(j) for j in input().split()] for i in range(NUM_ROWS)]
# Print a value:
# print(a)
c = int(input())
for i in range(NUM_ROWS):
    for j in range(NUM_COLS):
        a[i][j] = a[i][j] * c
for fila in a:
    for columna in fila:
        print(columna, end=' ')
    print()
