# Read a 2D list of integers:
# a = [[int(j) for j in input().split()] for i in range(NUM_ROWS)]
# Print a value:
# print(a)
m, n = [int(i) for i in input().split()]
for i in range(0,m):
  a = list(map(int,input().split()))
  if i == 0:
    maximal = max(a)
    j = [0, a.index(max(a))]
  elif max(a) > maximal:
    maximal = max(a)
    j = [i, a.index(max(a))]
print (*j,sep = ' ')