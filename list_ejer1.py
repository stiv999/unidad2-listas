# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
indice = 0
for i in a:
    if indice % 2 == 0:
        print(i, end=' ')
    indice += 1