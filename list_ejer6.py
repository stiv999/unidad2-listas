# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)

a.sort()
b = []

for i in a:
    if i not in b:
        b.append(i)

print(len(b))