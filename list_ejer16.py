# Read a 2D list of integers:
# a = [[int(j) for j in input().split()] for i in range(NUM_ROWS)]
# Print a value:
# print(a)
n = int(input())
a = [[abs(i - j) for j in range(n)] for i in range(n)]
for row in a:
  print(' '.join([str(i) for i in row]))