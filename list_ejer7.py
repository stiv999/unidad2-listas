# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)
for i in range(0, len(a), 2):
  popval = a.pop(i)
  a.insert(i+1, popval)
print(a)